using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using Vitalograph.Assesment.Core;
using Vitalograph.Assesment.Shifters;

namespace Vitalograph.Tests
{
    [TestClass]
    public class CharShift_Tests
    {
        private readonly ICapitalLetterShifter shifter;
        private readonly ICapitalLetterShifter oldShifter;
        public CharShift_Tests()
        {
            // Arrange
            shifter = new CapitalLetterShifter();
            oldShifter = new OldPerformanceShifter();
        }

        [TestMethod]
        public void Move_SpacesInput_ReturnsEmpty()
        {
            // Act & Assert
            Assert.AreEqual(string.Empty, shifter.MoveAsync("", 5).Result);
        }

        [TestMethod]
        public void Move_InputWithInvalidCharacters_ThrowsException()
        {
            Assert.ThrowsException<Exception>(() => shifter.MoveAsync("abc4a", 5).Result);
        }

        [TestMethod]
        public void Move_ValidCharacters_ReturnsShiftedCharacters()
        {
            Assert.AreEqual(shifter.MoveAsync("ABCD", 5).Result, "FGHI");
        }

        [TestMethod]
        public void Move_DifferentCallsWithDifferentPlaces_ReturnsShiftedCharacters()
        {
            var t1 = shifter.MoveAsync("ABCDE", 4);
            var t2 = shifter.MoveAsync("BACGD", 5);
            var t3 = shifter.MoveAsync("DEABC", 4);

            Assert.AreEqual(t1.Result,"EFGHI");
            Assert.AreEqual(t2.Result, "GFHLI");
            Assert.AreEqual(t3.Result, "HIEFG");
        }

        [TestMethod]
        [DeploymentItem("SampleData1", "TestData")]
        public void Move_LargeData_ReturnsShiftedCharacters()
        {
            string testData = System.IO.File.ReadAllText(@"TestData\SampleData1.txt");

            var Shifted1 = shifter.MoveAsync(testData, 10).Result;

            var shifted2 = oldShifter.MoveAsync(testData, 10).Result;

            Assert.AreEqual(Shifted1, shifted2);
        }
        
    }
}
