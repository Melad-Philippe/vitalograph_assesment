﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Vitalograph.Assesment.Core;

namespace Vitalograph.Assesment.Shifters
{
    public class CapitalLetterShifter : ICapitalLetterShifter
    {
        private readonly ConcurrentDictionary<CharNumbersShifted, char> _map;
        public CapitalLetterShifter()
        {
            _map = new ConcurrentDictionary<CharNumbersShifted, char>();
        }
        public Task<string> MoveAsync(string word, int n)
        {
           string output = string.Empty;

            if (string.IsNullOrWhiteSpace(word))
                return Task.FromResult(output);

            
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] < 65 || word[i] > 90)
                {
                    throw new Exception("Only A-Z supported.");
                }
                // Retrieve from dictionary 
                char val;
                if (_map.TryGetValue(new CharNumbersShifted(word[i],n), out val))
                {
                    output += val;
                    continue;
                }
                int shifted = word[i] + n;
                if (shifted > 90)
                {
                    shifted = 65 + shifted - 91;
                }

                var shiftedChar = (char)shifted;
                // store to the dictionary
                _map.TryAdd(new CharNumbersShifted(word[i], n), shiftedChar);
                output += shiftedChar;
            }

            return Task.FromResult(output);
        }


        private class CharNumbersShifted
        {
            public CharNumbersShifted(char letter, int shift)
            {
                _shift = shift;
                _letter = letter;
            }
            public int _shift { get; set; }
            public char _letter { get; set; }

            public override int GetHashCode()
            {
                return _shift.GetHashCode() + _letter.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                CharNumbersShifted _obj = obj as CharNumbersShifted;
                if (_obj is null) return false;
                return this.GetHashCode() == _obj.GetHashCode();
                
            }
        }
    }
}
