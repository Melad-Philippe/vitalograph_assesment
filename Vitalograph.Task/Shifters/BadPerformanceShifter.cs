﻿using System;
using System.Threading.Tasks;
using Vitalograph.Assesment.Core;

namespace Vitalograph.Assesment.Shifters
{
    public class OldPerformanceShifter : ICapitalLetterShifter
    {
        public Task<string> MoveAsync(string input, int n)
        {
            int shift = n;
            string output = "";
            
            
            if (input != null)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    if (input[i] < 65 || input[i] > 90)
                    {
                        throw new Exception("Only A-Z supported.");
                    }
                    int shifted = input[i] + shift;
                    if (shifted > 90)
                    {
                        shifted = 65 + shifted - 91;
                    }
                    output = output + (char)shifted;
                }
            }

            return Task.FromResult(output);
        }
    }
}
