﻿
using System.Threading.Tasks;

namespace Vitalograph.Assesment.Core
{
    public interface ICapitalLetterShifter
    {
        /// <summary>
        ///  Shifts the characters A to Z by N places
        /// </summary>
        /// <param name="word"> the word to shift its charaters </param>
        /// <param name="n"> number of places </param>
        /// <returns> New word shifted n places </returns>
        /// <Exception cref="System.Exception"> When Word contain non Capital Letters </Exception>: 
        public  Task<string> MoveAsync(string word, int n);
    }
}
