﻿using System;
using Vitalograph.Assesment.Core;
using Vitalograph.Assesment.Shifters;

namespace Vitalograph.Assesment

{
    class Program
    {
        static void Main(string[] args)
        {
            int places = 5;
            Console.Write("Input: ");
            string input = Console.ReadLine();

            ICapitalLetterShifter charShift = new CapitalLetterShifter();
       
            Console.WriteLine("Output: " + charShift.MoveAsync(input, places).Result);
        }
    }
}
